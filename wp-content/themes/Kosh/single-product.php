<?php get_header(); ?>

<?php
$WCProduct = new WC_Product(get_the_ID());
?>

<div class="ps-product--detail pt-60">
  <div class="ps-container">
    <div class="row">
      <div class="col-lg-10 col-md-12 col-lg-offset-1">

        <!--        --><?php //echo render('popup/cart-content.php'); ?>

        <?php
        $imagesGallery = get_field('gallery');
        ?>
        <?php if (!empty($imagesGallery)) : ?>
          <div class="ps-product__thumbnail">
            <div class="ps-product__preview">
              <div class="ps-product__variants">
                <?php foreach ($imagesGallery as $image) : ?>
                  <div class="item">
                    <img src="<?php echo $image['sizes']['shop_thumbnail']; ?>" alt="<?php echo $image['alt']; ?>">
                  </div>
                <?php endforeach; ?>
              </div>
            </div>
            <div class="ps-product__image">
              <?php foreach ($imagesGallery as $image) : ?>
                <div class="item">
                  <img class="zoom" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"
                       data-zoom-image="<?php echo $image['url']; ?>">
                </div>
              <?php endforeach; ?>
            </div>
          </div>

          <div class="ps-product__thumbnail--mobile">
            <div class="ps-product__main-img mobile-primary-slider">
              <?php foreach ($imagesGallery as $image) : ?>
                <div class="item">
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                </div>
              <?php endforeach; ?>
            </div>
<!--            <div class="ps-product__preview owl-slider mobile-primary-slider-bred">-->
<!--              --><?php //foreach ($imagesGallery as $image) : ?>
<!--                <div class="item">-->
<!--                  <img src="--><?php //echo $image['url']; ?><!--" alt="--><?php //echo $image['alt']; ?><!--">-->
<!--                </div>-->
<!--              --><?php //endforeach; ?>
<!--            </div>-->
          </div>
        <?php endif; ?>

        <div class="ps-product__info">

          <?php if (false) : ?>
            <div class="ps-product__rating">
              <select class="ps-rating">
                <option value="1">1</option>
                <option value="1">2</option>
                <option value="1">3</option>
                <option value="1">4</option>
                <option value="2">5</option>
              </select><a href="#">(Read all 8 reviews)</a>
            </div>
          <?php endif; ?>

          <h1><?php echo get_the_title(); ?></h1>
          <?php
          $terms = wp_get_post_terms(get_the_ID(), "product_cat");
          ?>
          <?php if (!empty($terms)) : ?>
            <p class="ps-product__category">
              <?php foreach ($terms as $term) : ?>
                <a href="<?php echo get_term_link($term->term_id); ?>"><?php echo $term->name; ?> </a>
              <?php endforeach; ?>
            </p>
          <?php endif; ?>


          <?php
          /**
           * SALE PRISE AND %
           * @var $wcProduct WC_Product
           */
          $wcProduct = wc_get_product(get_the_ID());

          if ($wcProduct instanceof WC_Product_Variable) {
            $price = $wcProduct->get_price();
            $price_sale = '';
          } else {
            $price = get_post_meta(get_the_ID(), '_regular_price', true);
            $price_sale = get_post_meta(get_the_ID(), '_sale_price', true);
          }
          ?>

          <h3 class="ps-product__price" data-price-html>
            <?php if ($price_sale !== '') : ?>
              <?= $price_sale . ' ' . get_woocommerce_currency_symbol(); ?>
              <del><?= $price . ' ' . get_woocommerce_currency_symbol(); ?></del>
            <?php else: ?>
              <?= $price . ' ' . get_woocommerce_currency_symbol(); ?>
              <del></del>
            <?php endif; ?>
          </h3>


          <?php
          $qDescr = get_field('shord_desc');
          ?>
          <?php if (isset($qDescr) && !empty($qDescr)) : ?>
            <div class="ps-product__block ps-product__quickview">
              <h4>ОПИСАНИЕ ТОВАРА</h4>
              <p>
                <?php echo $qDescr; ?>
              </p>
            </div>
          <?php endif; ?>

          <?php if ($wcProduct instanceof WC_Product_Variable) : ?>
            <?php
            /**
             * @var $wcProduct WC_Product_Variable
             */
            $variables = $wcProduct->get_available_variations();

            ?>
            <div class="ps-widget__content">
              <ul class="ps-list--color">

                <?php $isFirst = true; ?>
                <?php foreach ($variables as $color) : ?>
                  <?php
                  $attrs = $color['attributes'];

                  if (!isset($attrs['attribute_pa_color'])) continue;

                  $taxColorAttr = 'pa_color';
                  $termSlug = $attrs['attribute_pa_color'];

                  /**
                   * @var $term WP_Term
                   */
                  $term = get_term_by('slug', $termSlug, $taxColorAttr);
                  $customTermID = get_field('relation_color', $term);
                  if (empty($customTermID)) continue;
                  $customTermColorObj = get_term_by('id', $customTermID, 'color_tax');
                  $customTermColor = get_field('color_of_prod', $customTermColorObj);

                  $price = $color['display_regular_price'];
                  $price_sale = $color['display_price'];

                  ?>

                  <li>
                    <div class="round">
                      <input <?php echo $isFirst ? 'checked' : ''; ?>
                          type="radio" id="<?php echo $term->slug . '_' . $term->term_id; ?>"
                          name="pa_color"
                          value="<?php echo $color['variation_id']; ?>"
                          data-price-html-insert="
                            <?php if ($price_sale !== '') : ?>
                              <?= $price_sale . ' ' . get_woocommerce_currency_symbol(); ?>
                              <del><?= $price . ' ' . get_woocommerce_currency_symbol(); ?></del>
                            <?php else: ?>
                              <?= $price . ' ' . get_woocommerce_currency_symbol(); ?>
                              <del></del>
                            <?php endif; ?>
                          "
                      >
                      <label for="<?php echo $term->slug . '_' . $term->term_id; ?>" style="background-color: <?php echo $customTermColor; ?>; max-width: unset;">
                      </label>
                    </div>
                  </li>

                <?php $isFirst = false; ?>
                <?php endforeach; ?>

              </ul>
            </div>
          <?php endif; ?>

          <div class="ps-product__block ps-product__size">
            <h4>КОЛИЧЕСТВО</h4>
            <div class="form-group">
              <input class="form-control" type="number" value="1" product-data-count="1" min="1">
            </div>
          </div>

          <div class="ps-product__shopping">
            <a class="ps-btn mb-10" href="#" data-add-to-card product-data-id="<?php echo get_the_ID(); ?>">
              <span data-triger-display>КУПИТЬ</span>
              <i class="ps-icon-next" data-triger-display></i>
              <img style="display: none; height: 50px" src="<?php echo PHP_IMG; ?>/load.svg" alt="LOADING" data-triger-display>
            </a>

            <?php
            $userID = get_current_user_id() == 0 ? Alg_WC_Wish_List_Cookies::get_unlogged_user_id():get_current_user_id();
            $useIdFromUnloggedUser = get_current_user_id() == 0 ? true:false;
            ?>
            <div class="ps-product__actions">
              <div class="ps-product__actions" style="display: none; margin-top: 0px;" data-wish-loader>
                <img src="<?php echo PHP_IMG; ?>/loadearth.svg" alt="" style="height: 53px;">
              </div>
              <a class="mr-10 <?php echo Alg_WC_Wish_List_Item::is_item_in_wish_list(get_the_ID(), $userID, $useIdFromUnloggedUser) ? ' in-wish':'' ?>"
                 href="#" data-add-to-wish-list-btn="<?php echo get_the_ID(); ?>">
                <i class="ps-icon-heart"></i>
              </a>
              <?php if (false) : ?>
                <a href="compare.html">
                  <i class="ps-icon-share"></i>
                </a>
              <?php endif; ?>
            </div>

          </div>
        </div>


        <div class="clearfix"></div>
        <div class="ps-product__content mt-50">
          <ul class="tab-list" role="tablist">
            <li class="active"><a href="#info_product" aria-controls="info_product" role="tab" data-toggle="tab">Информация</a></li>
            <li><a href="#comments" aria-controls="comments" role="tab" data-toggle="tab">Комментарии</a></li>
          </ul>
        </div>
        <div class="tab-content mb-60">
          <div class="tab-pane active" role="tabpanel" id="info_product">
            <?php echo $WCProduct->get_short_description(); ?>
          </div>
          <div class="tab-pane" role="tabpanel" id="comments">
            <?php echo render('comments/comment.php', [
              'postID' => get_the_ID(),
            ]); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
