<?php


namespace KoshTheme\Service\OrderTelegram;


use KoshTheme\Forms\Admin\Telega\TelegaModel;
use Longman\TelegramBot\Request;
use Spatie\Emoji\Emoji;

class TelegramOrder
{

  private $telegram;

  public function __construct($telegram)
  {
    $this->telegram = $telegram;
  }

  public function sendMessageToAdmin($text)
  {
    $result = \Longman\TelegramBot\Request::sendMessage([
      'chat_id' => 424433523,
      'text'    => $text,
    ]);
  }

  public function sendOrderToSales($id)
  {
    try {

      $inline_keyboard = new \Longman\TelegramBot\Entities\InlineKeyboard([
        ['text' => 'Созвон Был', 'callback_data' => 'newordercallcomplate_' . $id],
      ]);

      $telegramModel = new TelegaModel();

      $order = wc_get_order($id);

      $sendedText =
        Emoji::greenHeart() . Emoji::greenHeart() . Emoji::greenHeart() .
        Emoji::greenHeart() . Emoji::greenHeart() . Emoji::greenHeart() .
        Emoji::greenHeart() . Emoji::greenHeart() . Emoji::greenHeart() .
        Emoji::greenHeart() . Emoji::greenHeart() . Emoji::greenHeart() .
        Emoji::greenHeart() . Emoji::greenHeart() . PHP_EOL . PHP_EOL .
        'Девченки! Новый товар подьехал.' .
        PHP_EOL .
        'Номер заказа - ' . $id .
        PHP_EOL .
        'Дата : ' . $order->order_date .
        PHP_EOL .
        'ОПЛАТА -' . ($order->get_payment_method() == '' ? 'NALOJKA' : ' ' . $order->get_payment_method()) .
        PHP_EOL;

      $sendedText .= 'Имя : ' . $order->get_billing_first_name() . PHP_EOL;
      $sendedText .= 'Фамилия : ' . $order->get_billing_last_name() . PHP_EOL;
      $sendedText .= 'Имейл : ' . $order->get_billing_email() . PHP_EOL;
      $sendedText .= 'Билл. Телефон : ' . $order->get_billing_phone() . PHP_EOL;
      $sendedText .= 'Город : ' . $order->get_shipping_city() . PHP_EOL;
      $sendedText .= 'Адресс НП. : ' . $order->get_shipping_address_1() . PHP_EOL;
      $sendedText .= 'Записка : ' . PHP_EOL . $order->get_customer_note() . PHP_EOL . PHP_EOL;

      foreach ($order->get_items() as $item_id => $item_data) {
        $product = $item_data->get_product();
        $product_name = $product->get_name();
        $item_quantity = $item_data->get_quantity();
        $item_total = $item_data->get_total();

        $sendedText .= PHP_EOL .
          'Название : ' . $product_name . PHP_EOL .
          'Количество : ' . $item_quantity . PHP_EOL .
          'Цена : ' . $item_total . PHP_EOL .
          '==================';
      }

      $sendedText .= PHP_EOL .
        'Всего ценник - ' . $order->get_total() . PHP_EOL . PHP_EOL .
        Emoji::greenHeart() . Emoji::greenHeart() . Emoji::greenHeart() .
        Emoji::greenHeart() . Emoji::greenHeart() . Emoji::greenHeart() .
        Emoji::greenHeart() . Emoji::greenHeart() . Emoji::greenHeart() .
        Emoji::greenHeart() . Emoji::greenHeart() . Emoji::greenHeart() .
        Emoji::greenHeart() . Emoji::greenHeart();

      foreach ( explode(',',$telegramModel->getSales()) as $saleID ) {

        $result = \Longman\TelegramBot\Request::sendMessage([
          'chat_id' => $saleID,
          'text'    => $sendedText,
          'reply_markup' => $inline_keyboard,
        ]);
      }

    } catch (Longman\TelegramBot\Exception\TelegramException $e) {
      container()->get('log.debug')->info($e->getMessage() . ' - ' . get_class(static::class));
    } catch (Longman\TelegramBot\Exception\TelegramLogException $e) {
      container()->get('log.debug')->info($e->getMessage() . ' - ' . get_class(static::class));
    }
  }

  public function editMessageThatCallWas($chatID, $messageID, $orderID)
  {
    try {

      $inline_keyboard = new \Longman\TelegramBot\Entities\InlineKeyboard([
        ['text' => 'Отменить', 'callback_data' => 'canceledorder_' . $orderID],
        ['text' => 'Послать', 'callback_data' => 'sendorder_' . $orderID]
      ]);

      $telegramModel = new TelegaModel();

      $order = wc_get_order($orderID);

      update_field('o_g_call', 'call_send', $order->get_id());

      $sendedText =
        Emoji::blueHeart() . Emoji::blueHeart() . Emoji::blueHeart() .
        Emoji::blueHeart() . Emoji::blueHeart() . Emoji::blueHeart() .
        Emoji::blueHeart() . Emoji::blueHeart() . Emoji::blueHeart() .
        Emoji::blueHeart() . Emoji::blueHeart() . Emoji::blueHeart() .
        Emoji::blueHeart() . Emoji::blueHeart() . PHP_EOL . PHP_EOL .
        'Новый товар подьехал.' .
        PHP_EOL .
        'Номер заказа - ' . $orderID .
        PHP_EOL .
        'Дата : ' . $order->order_date .
        PHP_EOL;

      $sendedText .= 'Имя : ' . $order->get_billing_first_name() . PHP_EOL;
      $sendedText .= 'Фамилия : ' . $order->get_billing_last_name() . PHP_EOL;
      $sendedText .= 'Имейл : ' . $order->get_billing_email() . PHP_EOL;
      $sendedText .= 'Билл. Телефон : ' . $order->get_billing_phone() . PHP_EOL;
      $sendedText .= 'Город : ' . $order->get_shipping_city() . PHP_EOL;
      $sendedText .= 'Адресс НП. : ' . $order->get_shipping_address_1() . PHP_EOL;
      $sendedText .= 'Записка : ' . PHP_EOL . $order->get_customer_note() . PHP_EOL . PHP_EOL;

      foreach ($order->get_items() as $item_id => $item_data) {
        $product = $item_data->get_product();
        $product_name = $product->get_name();
        $item_quantity = $item_data->get_quantity();
        $item_total = $item_data->get_total();

        $sendedText .= PHP_EOL .
          'Название : ' . $product_name . PHP_EOL .
          'Количество : ' . $item_quantity . PHP_EOL .
          'Цена : ' . $item_total . PHP_EOL .
          '==================';
      }

      $sendedText .= PHP_EOL .
        'Всего ценник - ' . $order->get_total() . PHP_EOL . PHP_EOL .
        Emoji::blueHeart() . Emoji::blueHeart() . Emoji::blueHeart() .
        Emoji::blueHeart() . Emoji::blueHeart() . Emoji::blueHeart() .
        Emoji::blueHeart() . Emoji::blueHeart() . Emoji::blueHeart() .
        Emoji::blueHeart() . Emoji::blueHeart() . Emoji::blueHeart() .
        Emoji::blueHeart() . Emoji::blueHeart();

      return Request::editMessageText([
        'chat_id'    => $chatID,
        'message_id' => $messageID,
        'text'       => $sendedText,
        'reply_markup' => $inline_keyboard,
      ]);

    } catch (Longman\TelegramBot\Exception\TelegramException $e) {
      container()->get('log.debug')->info($e->getMessage() . ' - ' . get_class(static::class));
    } catch (Longman\TelegramBot\Exception\TelegramLogException $e) {
      container()->get('log.debug')->info($e->getMessage() . ' - ' . get_class(static::class));
    }
  }

  public function canselOrderStatus($chatID, $messageID, $orderID)
  {
    try {

      $telegramModel = new TelegaModel();

      $order = wc_get_order($orderID);

      $order->update_status('wc-cancelled');

      $sendedText =
        Emoji::nauseatedFace() . Emoji::nauseatedFace() . Emoji::nauseatedFace() .
        Emoji::nauseatedFace() . Emoji::nauseatedFace() . Emoji::nauseatedFace() .
        Emoji::nauseatedFace() . Emoji::nauseatedFace() . Emoji::nauseatedFace() .
        Emoji::nauseatedFace() . Emoji::nauseatedFace() . Emoji::nauseatedFace() .
        Emoji::nauseatedFace() . Emoji::nauseatedFace() . PHP_EOL . PHP_EOL .
        'Отменет нахрен заказ.' .
        PHP_EOL .
        'Номер заказа - ' . $orderID .
        PHP_EOL .
        'Дата : ' . date('Y-m-d H:i:s') .
        PHP_EOL;

      $sendedText .= 'Имя : ' . $order->get_billing_first_name() . PHP_EOL;
      $sendedText .= 'Фамилия : ' . $order->get_billing_last_name() . PHP_EOL;
      $sendedText .= 'Билл. Телефон : ' . $order->get_billing_phone() . PHP_EOL;

      $sendedText .= PHP_EOL .
        'Всего ценник - ' . $order->get_total() . PHP_EOL . PHP_EOL .
        Emoji::nauseatedFace() . Emoji::nauseatedFace() . Emoji::nauseatedFace() .
        Emoji::nauseatedFace() . Emoji::nauseatedFace() . Emoji::nauseatedFace() .
        Emoji::nauseatedFace() . Emoji::nauseatedFace() . Emoji::nauseatedFace() .
        Emoji::nauseatedFace() . Emoji::nauseatedFace() . Emoji::nauseatedFace() .
        Emoji::nauseatedFace() . Emoji::nauseatedFace();

      return Request::editMessageText([
        'chat_id'    => $chatID,
        'message_id' => $messageID,
        'text'       => $sendedText,
      ]);

    } catch (Longman\TelegramBot\Exception\TelegramException $e) {
      container()->get('log.debug')->info($e->getMessage() . ' - ' . get_class(static::class));
    } catch (Longman\TelegramBot\Exception\TelegramLogException $e) {
      container()->get('log.debug')->info($e->getMessage() . ' - ' . get_class(static::class));
    }
  }

  public function finOrderSend($chatID, $messageID, $orderID)
  {
    try {

      $telegramModel = new TelegaModel();

      $order = wc_get_order($orderID);

      update_field('o_g_status_shipments', 'shipment_send', $order->get_id());

      $sendedText =
        Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() .
        Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() .
        Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() .
        Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() .
        Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() . PHP_EOL . PHP_EOL .
        'Заказ ПОслан Хуле :)' .
        PHP_EOL .
        'Номер заказа - ' . $orderID .
        PHP_EOL .
        'Дата : ' . date('Y-m-d H:i:s') .
        PHP_EOL;

      $sendedText .= 'Имя : ' . $order->get_billing_first_name() . PHP_EOL;
      $sendedText .= 'Фамилия : ' . $order->get_billing_last_name() . PHP_EOL;
      $sendedText .= 'Билл. Телефон : ' . $order->get_billing_phone() . PHP_EOL;

      $sendedText .= PHP_EOL .
        'Всего ценник - ' . $order->get_total() . PHP_EOL . PHP_EOL .
        Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() .
        Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() .
        Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() .
        Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss() .
        Emoji::faceBlowingAKiss() . Emoji::faceBlowingAKiss();

      return Request::editMessageText([
        'chat_id'    => $chatID,
        'message_id' => $messageID,
        'text'       => $sendedText,
      ]);

    } catch (Longman\TelegramBot\Exception\TelegramException $e) {
      container()->get('log.debug')->info($e->getMessage() . ' - ' . get_class(static::class));
    } catch (Longman\TelegramBot\Exception\TelegramLogException $e) {
      container()->get('log.debug')->info($e->getMessage() . ' - ' . get_class(static::class));
    }
  }

  public function sendOrderToManager($chatID, $messageID, $orderID)
  {
    try {

      $inline_keyboard = new \Longman\TelegramBot\Entities\InlineKeyboard([
        ['text' => 'Выслал!', 'callback_data' => 'sended_' . $orderID],
      ]);

      $telegramModel = new TelegaModel();

      $order = wc_get_order($orderID);

      $sendedText =
        Emoji::tongue() . Emoji::tongue() . Emoji::tongue() .
        Emoji::tongue() . Emoji::tongue() . Emoji::tongue() .
        Emoji::tongue() . Emoji::tongue() . Emoji::tongue() .
        Emoji::tongue() . Emoji::tongue() . Emoji::tongue() .
        Emoji::tongue() . Emoji::tongue() . PHP_EOL . PHP_EOL .
        'Новый товар подьехал.' .
        PHP_EOL .
        'Номер заказа - ' . $orderID .
        PHP_EOL .
        'Дата : ' . $order->order_date .
        PHP_EOL .
        'ОПЛАТА -' . ($order->get_payment_method() == '' ? 'NALOJKA' : ' ' . $order->get_payment_method()) .
        PHP_EOL;

      $sendedText .= 'Имя : ' . $order->get_billing_first_name() . PHP_EOL;
      $sendedText .= 'Фамилия : ' . $order->get_billing_last_name() . PHP_EOL;
      $sendedText .= 'Имейл : ' . $order->get_billing_email() . PHP_EOL;
      $sendedText .= 'Билл. Телефон : ' . $order->get_billing_phone() . PHP_EOL;
      $sendedText .= 'Город : ' . $order->get_shipping_city() . PHP_EOL;
      $sendedText .= 'Адресс НП. : ' . $order->get_shipping_address_1() . PHP_EOL;
      $sendedText .= 'Записка : ' . PHP_EOL . $order->get_customer_note() . PHP_EOL . PHP_EOL;

      foreach ($order->get_items() as $item_id => $item_data) {
        $product = $item_data->get_product();
        $product_name = $product->get_name();
        $item_quantity = $item_data->get_quantity();
        $item_total = $item_data->get_total();

        $sendedText .= PHP_EOL .
          'Название : ' . $product_name . PHP_EOL .
          'Количество : ' . $item_quantity . PHP_EOL .
          'Цена : ' . $item_total . PHP_EOL .
          '==================';
      }

      $sendedText .= PHP_EOL .
        'Всего ценник - ' . $order->get_total() . PHP_EOL . PHP_EOL .
        Emoji::tongue() . Emoji::tongue() . Emoji::tongue() .
        Emoji::tongue() . Emoji::tongue() . Emoji::tongue() .
        Emoji::tongue() . Emoji::tongue() . Emoji::tongue() .
        Emoji::tongue() . Emoji::tongue() . Emoji::tongue() .
        Emoji::tongue() . Emoji::tongue();

      foreach ( explode(',',$telegramModel->getManagers()) as $managerID ) {

        $result = \Longman\TelegramBot\Request::sendMessage([
          'chat_id' => $managerID,
          'text'    => $sendedText,
          'reply_markup' => $inline_keyboard,
        ]);

      }


      $sendedText =
        Emoji::tractor() . Emoji::tractor() . Emoji::tractor() .
        Emoji::tractor() . Emoji::tractor() . Emoji::tractor() .
        Emoji::tractor() . Emoji::tractor() . Emoji::tractor() .
        Emoji::tractor() . Emoji::tractor() . Emoji::tractor() .
        Emoji::tractor() . Emoji::tractor() . PHP_EOL . PHP_EOL .
        'Товар отправлен манагеру' .
        PHP_EOL .
        'Номер заказа - ' . $orderID .
        PHP_EOL .
        'Дата : ' . date('Y-m-d H:i:s') .
        PHP_EOL .
        'ОПЛАТА -' . ($order->get_payment_method() == '' ? 'NALOJKA' : ' ' . $order->get_payment_method()) .
        PHP_EOL;

      $sendedText .= 'Имя : ' . $order->get_billing_first_name() . PHP_EOL;
      $sendedText .= 'Фамилия : ' . $order->get_billing_last_name() . PHP_EOL;
      $sendedText .= 'Имейл : ' . $order->get_billing_email() . PHP_EOL;
      $sendedText .= 'Билл. Телефон : ' . $order->get_billing_phone() . PHP_EOL;
      $sendedText .= PHP_EOL .
        'Всего ценник - ' . $order->get_total() . PHP_EOL . PHP_EOL .
        Emoji::tractor() . Emoji::tractor() . Emoji::tractor() .
        Emoji::tractor() . Emoji::tractor() . Emoji::tractor() .
        Emoji::tractor() . Emoji::tractor() . Emoji::tractor() .
        Emoji::tractor() . Emoji::tractor() . Emoji::tractor() .
        Emoji::tractor() . Emoji::tractor();
      return Request::editMessageText([
        'chat_id'    => $chatID,
        'message_id' => $messageID,
        'text'       => $sendedText,
      ]);

    } catch (Longman\TelegramBot\Exception\TelegramException $e) {
      container()->get('log.debug')->info($e->getMessage() . ' - ' . get_class(static::class));
    } catch (Longman\TelegramBot\Exception\TelegramLogException $e) {
      container()->get('log.debug')->info($e->getMessage() . ' - ' . get_class(static::class));
    }
  }

}