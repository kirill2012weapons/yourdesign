<?php

namespace KoshTheme\Service\Shop\Engine;

abstract class FilterComponent
{

  abstract public function getRender();

}