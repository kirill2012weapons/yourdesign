<?php

namespace KoshTheme\Forms\User\Checkout;


use Symfony\Component\Validator\Constraints as Assert;
use KoshTheme\Validator\Constraints as AcmeAssert;

/**
 * @AcmeAssert\ContainsOrderExists(
 *     message="Проверьте информацию о заказе."
 * )
 */
class CheckoutFindOrderModel
{

  /**
   * @Assert\NotBlank(
   *   message="Обязательное поле."
   * )
   * @Assert\Regex(
   *   pattern="/^([0-9]){1,6}$/",
   *   message="Номер заказа состоит только из цифр и длинна не превышает 6 символов"
   * )
   */
  private $orderId;

  /**
   * @Assert\NotBlank(
   *   message="Обязательное поле."
   * )
   */
  private $orderHash;

  /**
   * @return mixed
   */
  public function getOrderId() {
    return $this->orderId;
  }

  /**
   * @param mixed $orderId
   */
  public function setOrderId($orderId) {
    $this->orderId = $orderId;
  }

  /**
   * @return mixed
   */
  public function getOrderHash() {
    return $this->orderHash;
  }

  /**
   * @param mixed $orderHash
   */
  public function setOrderHash($orderHash) {
    $this->orderHash = $orderHash;
  }

}