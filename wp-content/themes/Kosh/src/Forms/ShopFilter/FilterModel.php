<?php

namespace KoshTheme\Forms\ShopFilter;


use KoshTheme\Validator\Constraints\ContainsUserEmailIsExists;
use Symfony\Component\Validator\Constraints as Assert;
use KoshTheme\Validator\Constraints as AcmeAssert;

class FilterModel
{

  private $categories;

  private $priceMin;
  private $priceMax;

  private $tint;

  private $color;

  private $sortedBy;

  /**
   * @return mixed
   */
  public function getSortedBy()
  {
    return $this->sortedBy;
  }

  /**
   * @param mixed $sortedBy
   */
  public function setSortedBy($sortedBy)
  {
    $this->sortedBy = $sortedBy;
  }

  /**
   * @return mixed
   */
  public function getTint()
  {
    return $this->tint;
  }

  /**
   * @param mixed $tint
   */
  public function setTint($tint)
  {
    $this->tint = $tint;
  }

  /**
   * @return mixed
   */
  public function getColor()
  {
    return $this->color;
  }

  /**
   * @param mixed $color
   */
  public function setColor($color)
  {
    $this->color = $color;
  }

  /**
   * @return mixed
   */
  public function getPriceMin()
  {
    return $this->priceMin;
  }

  /**
   * @param mixed $priceMin
   */
  public function setPriceMin($priceMin)
  {
    $this->priceMin = $priceMin;
  }

  /**
   * @return mixed
   */
  public function getPriceMax()
  {
    return $this->priceMax;
  }

  /**
   * @param mixed $priceMax
   */
  public function setPriceMax($priceMax)
  {
    $this->priceMax = $priceMax;
  }

  /**
   * @return mixed
   */
  public function getCategories()
  {
    return $this->categories;
  }

  /**
   * @param mixed $categories
   */
  public function setCategories($categories)
  {
    $this->categories = $categories;
  }

}