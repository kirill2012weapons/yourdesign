<?php
/**
 * @var $tint \Symfony\Component\Form\FormView
 */
?>
<?php if (isset($tint) && !empty($tint->vars['choices'])) : ?>

  <aside class="ps-widget--sidebar ps-widget--category">
    <div class="ps-widget__header">
      <h3>Оттенок</h3>
    </div>
    <div class="ps-widget__content" style="margin-bottom: 5px;">
      <ul class="ps-list--checked tints">

        <?php $count = 1; ?>
        <?php foreach ($tint->vars['choices'] as $choice) : ?>

          <li>
            <div class="round">
              <input data-cat-change
                type="radio"
                     id="<?php echo $tint->vars['id'] . '_' . $count; ?>"
                     name="<?php echo $tint->vars['full_name'] ?>"
                     value="<?php echo $choice->value; ?>"
                <?php echo $choice->value == $tint->vars['value'] ? ' checked' : ''; ?>
              />
              <label for="<?php echo $tint->vars['id'] . '_' . $count; ?>">
                <span><?php echo $choice->label; ?></span>
              </label>
            </div>
          </li>

          <?php $count++; ?>
        <?php endforeach; ?>

      </ul>
    </div>
  </aside>

<?php endif; ?>