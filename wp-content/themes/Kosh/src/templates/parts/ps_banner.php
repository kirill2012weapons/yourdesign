<?php if (have_rows('mp_g_msl_r', 'opt_st_id')) : ?>

  <div class="ps-banner">
    <div class="rev_slider fullscreenbanner" id="home-banner">
      <ul>

        <?php while (have_rows('mp_g_msl_r', 'opt_st_id')) : the_row(); ?>
          <li class="ps-banner" data-index="rs-<?php echo get_row_index(); ?>" data-transition="random" data-slotamount="default"
              data-hideafterloop="0" data-hideslideonmobile="off" data-rotate="0">

            <?php if (get_sub_field('image')) : ?>

              <img class="rev-slidebg lazy" src="<?= get_sub_field('image')['url']; ?>" alt="" data-bgposition="center center" data-bgfit="cover"
                   data-bgrepeat="no-repeat" data-bgparallax="5" data-no-retina>

            <?php endif; ?>

            <?php if (get_sub_field('pre_title')) : ?>

              <div class="tp-caption ps-banner__header" id="layer-1" data-x="left" data-hoffset="['-60','15','15','15']"
                   data-y="['middle','middle','middle','middle']" data-voffset="['-150','-120','-150','-170']"
                   data-width="['none','none','none','400']" data-type="text" data-responsive_offset="on"
                   data-frames="[{&quot;delay&quot;:1000,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:50px;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;x:50px;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]">
                <p><?php echo get_sub_field('pre_title'); ?></p>
              </div>

            <?php endif; ?>

            <?php if (get_sub_field('title')) : ?>

              <div class="tp-caption ps-banner__title" id="layer21" data-x="['left','left','left','left']" data-hoffset="['-60','15','15','15']"
                   data-y="['middle','middle','middle','middle']" data-voffset="['-60','-40','-50','-70']" data-type="text"
                   data-responsive_offset="on" data-textAlign="['center','center','center','center']"
                   data-frames="[{&quot;delay&quot;:1200,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:50px;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;x:50px;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]">
                <p class="text-uppercase"><?php echo get_sub_field('title'); ?></p>
              </div>

            <?php endif; ?>

            <?php if (get_sub_field('description')) : ?>

              <div class="tp-caption ps-banner__description" id="layer211" data-x="['left','left','left','left']"
                   data-hoffset="['-60','15','15','15']" data-y="['middle','middle','middle','middle']" data-voffset="['30','50','50','50']"
                   data-type="text" data-responsive_offset="on" data-textAlign="['center','center','center','center']"
                   data-frames="[{&quot;delay&quot;:1200,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:50px;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;x:50px;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]">
                <p><?php echo get_sub_field('description'); ?></p>
              </div>

            <?php endif; ?>

            <?php if (get_sub_field('btn')) : ?>

              <a class="tp-caption ps-btn" id="layer31" href="<?php echo get_sub_field('btn')['url'] ?>" data-x="['left','left','left','left']" data-hoffset="['-60','15','15','15']"
                 data-y="['middle','middle','middle','middle']" data-voffset="['120','140','200','200']" data-type="text" data-responsive_offset="on"
                 data-textAlign="['center','center','center','center']"
                 data-frames="[{&quot;delay&quot;:1500,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:50px;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;x:50px;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]">
                <?php echo get_sub_field('btn')['title'] ?><i class="ps-icon-next"></i>
              </a>

            <?php endif; ?>

          </li>
        <?php endwhile; ?>

      </ul>
    </div>
  </div>

<?php endif; ?>