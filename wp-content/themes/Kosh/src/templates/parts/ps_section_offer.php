<?php
$items = get_field('mpti_g', 'opt_st_id');
?>

<?php if (!empty($items)) : ?>

<div class="ps-section--offer">

  <?php if (isset($items['image_1']) && !empty($items['image_1'])) : ?>
    <div class="ps-column">
      <a class="ps-offer" href="<?php echo isset($items['link_1']) && !empty($items['link_1']) ? $items['link_1']['url'] : '#'; ?>">
        <img class="lazy" src="<?= $items['image_1']['sizes']['custom_size_958_401']; ?>" alt="">
      </a>
    </div>
  <?php endif; ?>

  <?php if (isset($items['image_2']) && !empty($items['image_2'])) : ?>
    <div class="ps-column">
        <a class="ps-offer" href="<?php echo isset($items['link_2']) && !empty($items['link_2']) ? $items['link_2']['url'] : '#'; ?>">
          <img class="lazy" src="<?= $items['image_2']['sizes']['custom_size_958_401']; ?>" alt="">
        </a>
    </div>
  <?php endif; ?>

</div>

<?php endif; ?>