<div class="ps-cart">

  <a class="ps-cart__toggle" href="#">
    <span><i data-count-item-mini-cart><?php echo WC()->cart->get_cart_contents_count(); ?></i></span>
    <i class="ps-icon-shopping-cart"></i>
  </a>

  <div class="ps-cart__listing">

    <div class="ps-cart__content" data-item-content-wrapper>

      <?php echo render('mini-card/item-template.php'); ?>

    </div>

    <div class="ps-cart__total">
      <p>Всего продуктов:<span data-count-item-mini-cart><?php echo WC()->cart->get_cart_contents_count(); ?></span></p>
      <p>Общая стоимость:<span data-total-item-cost-cart><?php echo WC()->cart->get_cart_total(); ?></span></p>
    </div>

    <div class="ps-cart__footer"><a class="ps-btn" href="<?php echo home_url('/checkout/'); ?>">ОПЛАТИТЬ<i class="ps-icon-arrow-left"></i></a>
    </div>
  </div>
</div>