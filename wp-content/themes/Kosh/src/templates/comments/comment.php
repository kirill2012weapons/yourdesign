<?php

use KoshTheme\Forms\Comment\CommentModel;
use KoshTheme\Forms\Comment\CommentType;

/**
 * @var $postID int
 */

$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
/**
 * @var $formFactory \Symfony\Component\Form\FormFactory
 */
$formFactory = container()->get('factory.form.annotation_validator');

$commentModel = new CommentModel();
$commentModel->setPostId($postID);

if (is_user_logged_in()) {

  $curUser = wp_get_current_user();
  $currUserMeta = (object) get_user_meta($curUser->ID);
  if (isset($currUserMeta->first_name)) $commentModel->setName($currUserMeta->first_name[0]);
  if (isset($currUserMeta->last_name)) $commentModel->setSurname($currUserMeta->last_name[0]);
  $commentModel->setEmail($curUser->get('user_email'));

}

$commentForm = $formFactory->createBuilder(CommentType::class, $commentModel)->getForm();

$commentForm->handleRequest($request);

if ($commentForm->isSubmitted() && $commentForm->isValid()) {

  $commentdata = array(
    'comment_post_ID' => $commentModel->getPostId(),
    'comment_author' => $commentModel->getName() . ' ' . $commentModel->getSurname(),
    'comment_author_email' => $commentModel->getEmail(),
    'comment_content' => $commentModel->getComment(),
    'comment_type' => '',
    'user_ID' => get_current_user_id(),
  );

  wp_new_comment($commentdata);

  global $wp;
  wp_safe_redirect( home_url($wp->request) );
  die();

}

/**
 * @var $commentFormView \Symfony\Component\Form\FormView
 */
$commentFormView = $commentForm->createView();

$commArgs = array(
  'number' => 30,
  'orderby' => 'comment_date',
  'order' => 'ASC',
  'type' => '',
  'status' => 'approve',
  'post_id' => $postID,
);

$comments = get_comments($commArgs);

?>

<?php if (!empty($comments)) : ?>

  <?php foreach ($comments as $comment) : ?>
    <?php
    /**
     * @var $comment WP_Comment
     */
    ?>

    <div class="ps-review">
      <div class="ps-review__thumbnail">
        <?php if ($comment->user_id > 0) : ?>
          <img src="<?php echo get_avatar_url($comment->user_id); ?>" alt="Avatar">
        <?php else: ?>
          <img src="<?php echo PHP_IMG; ?>/anonimimage.jpeg" alt="">
        <?php endif; ?>
      </div>
      <div class="ps-review__content">
        <header>
          <p>Отправил<a href=""> <?php echo $comment->comment_author; ?></a> - <?php echo $comment->comment_date_gmt; ?></p>
        </header>
        <p>
          <?php echo nl2br($comment->comment_content); ?>
        </p>
      </div>
    </div>

    <?php endforeach; ?>

<?php endif; ?>


<form class="ps-product__review" action="?" method="post">
  <input type="hidden"
         name="<?= $commentFormView->children['postId']->vars['full_name'] ?>"
         value="<?= $commentFormView->children['postId']->vars['value'] ?>"
  >
  <h4>Добавить комментарий</h4>
  <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

      <div class="form-group">
        <label>Комментарий:</label>
        <?php if ($commentFormView->children['comment']->vars['errors']->count() > 0) : ?>
          <?php foreach ($commentFormView->children['comment']->vars['errors'] as $error) : ?>
            <div style="margin-bottom: 3px;">
              <span style="color: #ff0013; font-size: 12px;">
                  <?= $error->getMessage(); ?>
              </span>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>
        <textarea name="<?= $commentFormView->children['comment']->vars['full_name'] ?>" class="form-control"
                  rows="6"><?= $commentFormView->children['comment']->vars['value'] ?></textarea>
      </div>

      <div class="form-group">
        <button type="submit" class="ps-btn ps-btn--sm">Отправить<i class="ps-icon-next"></i></button>
      </div>

    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

      <div class="form-group" <?php echo is_user_logged_in() ? 'style="display:none;"':''; ?>>
        <label>Имя:<span>*</span></label>
        <?php if ($commentFormView->children['name']->vars['errors']->count() > 0) : ?>
          <?php foreach ($commentFormView->children['name']->vars['errors'] as $error) : ?>
            <div style="margin-bottom: 3px;">
              <span style="color: #ff0013; font-size: 12px;">
                  <?= $error->getMessage(); ?>
              </span>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>
        <input class="form-control"
               type="text" placeholder=""
               name="<?= $commentFormView->children['name']->vars['full_name'] ?>"
               value="<?= $commentFormView->children['name']->vars['value'] ?>"
        >
      </div>

      <div class="form-group" <?php echo is_user_logged_in() ? 'style="display:none;"':''; ?>>
        <label>Фамилия:<span>*</span></label>
        <?php if ($commentFormView->children['surname']->vars['errors']->count() > 0) : ?>
          <?php foreach ($commentFormView->children['surname']->vars['errors'] as $error) : ?>
            <div style="margin-bottom: 3px;">
              <span style="color: #ff0013; font-size: 12px;">
                  <?= $error->getMessage(); ?>
              </span>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>
        <input class="form-control"
               type="text"
               placeholder=""
               name="<?= $commentFormView->children['surname']->vars['full_name'] ?>"
               value="<?= $commentFormView->children['surname']->vars['value'] ?>"
        >
      </div>

      <div class="form-group" <?php echo is_user_logged_in() ? 'style="display:none;"':''; ?>>
        <label>Email:<span>*</span></label>
        <?php if ($commentFormView->children['email']->vars['errors']->count() > 0) : ?>
          <?php foreach ($commentFormView->children['email']->vars['errors'] as $error) : ?>
            <div style="margin-bottom: 3px;">
              <span style="color: #ff0013; font-size: 12px;">
                  <?= $error->getMessage(); ?>
              </span>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>
        <input class="form-control"
               type="email" placeholder=""
               name="<?= $commentFormView->children['email']->vars['full_name'] ?>"
               value="<?= $commentFormView->children['email']->vars['value'] ?>"
        >
      </div>

    </div>
  </div>
</form>