<?php

/******************************************************************
 *
 * img-list-k
 *
 ******************************************************************/

/******************************************************************
 *
 * ─────────▄▄───────────────────▄▄──
 * ──────────▀█───────────────────▀█─
 * ──────────▄█───────────────────▄█─
 * ──█████████▀───────────█████████▀─
 * ───▄██████▄─────────────▄██████▄──
 * ─▄██▀────▀██▄─────────▄██▀────▀██▄
 * ─██────────██─────────██────────██
 * ─██───██───██─────────██───██───██
 * ─██────────██─────────██────────██
 * ──██▄────▄██───────────██▄────▄██─
 * ───▀██████▀─────────────▀██████▀──
 * ──────────────────────────────────
 * ──────────────────────────────────
 * ──────────────────────────────────
 * ───────────█████████████──────────
 * ──────────────────────────────────
 * ──────────────────────────────────
 *
 * CHANGES FUCKING
 *      | Creating Templating for
 *      | products on the front page
 *
 ******************************************************************/

$argsCategory = array(
  'taxonomy'      => 'product_cat',
  'orderby'       => 'name',
  'show_count'    => true,
  'hierarchical'  => true,
  'hide_empty'    => true,
  'parent'        => 0,
);
$allCategories = get_categories($argsCategory);

?>

<?php if (!empty($allCategories)) : ?>

  <?php
    $categoryIterator = 1;
  ?>

  <?php foreach ($allCategories as $cat) : ?>

    <?php
    /**
     * @var $cat WP_Term
     */
    ?>

    <div class="ps-section--features-product ps-section masonry-root pt-100 pb-100">
      <div class="ps-container">
        <div class="ps-section__header mb-50">
          <h3 class="ps-section__title" data-mask="КАТЕГОРИЯ">- <?php echo $cat->name; ?></h3>
          <ul class="ps-masonry__filter">

            <?php

            $categoriesChildren = get_categories([
              'taxonomy'      => 'product_cat',
              'parent'        => $cat->cat_ID,
              'orderby'       => 'name',
              'show_count'    => true,
              'hide_empty'    => true,
            ]);
            /* Add parent category to the stack */
            array_unshift($categoriesChildren, $cat);
            /**
             * @var $category WP_Term
             * current category
             */
            $currentOnInit = 'current';
            ?>
            <?php foreach ($categoriesChildren as $category) : ?>

              <li class="<?= $currentOnInit; ?>">
                <a href="#" data-filter="<?= '.' . $category->slug . $categoryIterator; ?>">
                  <?= $category->name; ?>
                  <sup><?= $category->count; ?></sup>
                </a>
              </li>

              <?php
              $currentOnInit = '';
              ?>
            <?php endforeach; ?>

          </ul>
        </div>
        <div class="ps-section__content pb-50">
          <div class="masonry-wrapper" data-col-md="4" data-col-sm="2" data-col-xs="1" data-gap="30" data-radio="100%">
            <div class="ps-masonry">
              <div class="grid-sizer"></div>

              <?php foreach ($categoriesChildren as $category) : ?>

                <?php
                $productsArgsFromCar = [
                  'post_type' => 'product',
                  'post_status' => 'publish',
                  'ignore_sticky_posts' => true,
                  'posts_per_page' => -1,
                  'tax_query' => [
                    [
                      'taxonomy' => 'product_cat',
                      'field' => 'term_id',
                      'terms' => $category->term_id,
                      'operator' => 'IN'
                    ]
                  ],
                  'meta_query' => [
                    [
                      'key' => '_stock_status',
                      'value' => 'instock',
                      'compare' => '='
                    ]
                  ]
                ];

                $productsFromCar = new WP_Query($productsArgsFromCar);

                ?>

                <?php while ($productsFromCar->have_posts()) : $productsFromCar->the_post(); ?>

                  <div class="grid-item <?= $category->slug . $categoryIterator; ?>">
                    <div class="grid-item__content-wrapper">
                      <div class="ps-shoe mb-30">
                        <div class="ps-shoe__thumbnail">

                          <?php
                          /**
                           * Bage - NEW
                           */
                          $isNew = get_field('is_new', get_the_ID());
                          ?>
                          <?php if ($isNew) : ?>
                            <div class="ps-badge">
                              <span>New</span>
                            </div>
                          <?php endif; ?>

                          <?php
                          /**
                           * SALE PRISE AND %
                           * @var $wcProduct WC_Product
                           */

                          $wcProduct = wc_get_product(get_the_ID());

                          if ($wcProduct instanceof WC_Product_Variable) {
                            $price = $wcProduct->get_price();
                            $price_sale = '';
                          } else {
                            $price = get_post_meta(get_the_ID(), '_regular_price', true);
                            $price_sale = get_post_meta(get_the_ID(), '_sale_price', true);
                          }

                          ?>
                          <?php if ($price_sale !== '') : ?>
                            <div class="ps-badge ps-badge--sale <?php echo $isNew ? 'ps-badge--2nd':''; ?>">
                              <span>-<?= ceil((($price - $price_sale) / $price) * 100) ?>%</span>
                            </div>
                          <?php endif; ?>

                          <?php
                          $userID = get_current_user_id() == 0 ? Alg_WC_Wish_List_Cookies::get_unlogged_user_id() : get_current_user_id();

                          $useIdFromUnloggedUser = get_current_user_id() == 0 ? true : false;
                          ?>
                          <div class="ps-shoe__favorite" style="display: none;" data-wish-loader>
                            <img src="<?php echo PHP_IMG; ?>/loadearth.svg" alt="">
                          </div>
                          <a class="ps-shoe__favorite <?php echo Alg_WC_Wish_List_Item::is_item_in_wish_list(get_the_ID(), $userID, $useIdFromUnloggedUser) ? ' in-wish' : '' ?>" href="#" data-add-to-wish-list-btn="<?php echo get_the_ID(); ?>">
                            <i class="ps-icon-heart"></i>
                          </a>
                          <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'custom_size_800_800'); ?>" class="lazy" alt="">
                          <a class="ps-shoe__overlay" href="<?php echo get_permalink( get_the_ID() ); ?>"></a>
                        </div>
                        <div class="ps-shoe__content">
                          <div class="ps-shoe__variants">

                            <?php if (get_field('gallery', get_the_ID())) : ?>

                              <?php
                              $gallery = get_field('gallery', get_the_ID());
                              ?>

                              <div class="ps-shoe__variant normal">

                                <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'custom_size_800_800'); ?>" class="lazy" alt="">

                                <?php foreach ($gallery as $image) : ?>

                                  <img src="<?= $image['sizes']['custom_size_800_800']; ?>" class="lazy" alt="">

                                <?php endforeach; ?>

                              </div>

                            <?php endif; ?>

                          </div>
                          <div class="ps-shoe__detail"><a class="ps-shoe__name" href="<?php echo get_permalink( get_the_ID() ); ?>"><?= get_the_title(); ?></a>
                            <!--                                                    <p class="ps-shoe__categories">-->
                            <!--                                                        <a href="#">Men shoes</a>,-->
                            <!--                                                        <a href="#"> Nike</a>,-->
                            <!--                                                        <a href="#"> Jordan</a>-->
                            <!--                                                    </p>-->
                            <span class="ps-shoe__price">
                                                          <?php if ($price_sale !== '') : ?>
                                                            <del><?= $price . ' ' . get_woocommerce_currency_symbol(); ?></del> <?= $price_sale . ' ' . get_woocommerce_currency_symbol(); ?>
                                                          <?php else: ?>
                                                            <del></del><?= $price . ' ' . get_woocommerce_currency_symbol(); ?>
                                                          <?php endif; ?>
                                                      </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                <?php endwhile; ?>

              <?php endforeach; ?>

            </div>
          </div>
        </div>
      </div>
    </div>

    <?php
    $categoryIterator++;
    ?>

  <?php endforeach; ?>

<?php endif; ?>