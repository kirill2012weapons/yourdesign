<?php
/**
 * @var $curUser WP_User
 * @var $currUserMeta object
 */
?>

<?php
$orderStatuses = ['wc-on-hold', 'wc-processing', 'wc-completed', 'wc-cancelled', 'wc-failed'];

$customerUserId = get_current_user_id();

$customerOrders = wc_get_orders( [
  'meta_key' => '_customer_user',
  'meta_value' => $customerUserId,
  'post_status' => $orderStatuses,
  'numberposts' => -1
] );
?>

<?php if (!empty($customerOrders)) : ?>
    <?php foreach ($customerOrders as $order) : ?>
    <div class="ps-cart-listing ps-table--compare">
      <?php
      /**
       * @var $order Order
       */
      ?>
      <table class="table ps-cart__table">
        <tbody>

        <tr>
          <td style="width: 30%;">Заказ #</td>
          <td>
            <?php echo $order->get_id(); ?>
          </td>
        </tr>

        <tr>
          <td style="width: 30%;">Хэш заказа</td>
          <td>
            <?php echo $order->get_order_key(); ?>
          </td>
        </tr>

        <tr>
          <td style="width: 30%;">Имя Фамилия</td>
          <td><span class="price"><?php echo $order->get_billing_first_name(); ?> <?php echo $order->get_billing_last_name(); ?></span></td>
        </tr>

        <tr>
          <td style="width: 30%;">Номер Телефона</td>
          <td>
            <?php echo $order->get_billing_phone(); ?>
          </td>
        </tr>

        <tr>
          <td style="width: 30%;">Сумма покупки</td>
          <td><span class="price"><?php echo $order->get_total() . ' ' . get_woocommerce_currency_symbol(); ?></span></td>
        </tr>

        <tr>
          <td style="width: 30%;">Статус заказа</td>
          <td>
            <?php echo $order->get_status(); ?>
          </td>
        </tr>

        <tr>
          <td style="width: 30%;">Город</td>
          <td>
            <?php echo $order->get_shipping_city(); ?>
          </td>
        </tr>

        <tr>
          <td style="width: 30%;">Адресс</td>
          <td>
            <?php echo $order->get_shipping_address_1(); ?>
          </td>
        </tr>

        <tr>
          <td style="width: 30%;">Статус отправки</td>
          <td>
            <?php
            $status = get_field('o_g_status_shipments', $order->get_id())['label'];
            if (is_null($status)) $status = 'Не отправлен';
            echo $status;
            ?>
          </td>
        </tr>

        <tr>
          <td style="width: 30%;">Способ оплаты</td>
          <td>
            <span class="status">
              <?php
              if ($order->get_payment_method() == 'liqpay-webplus') echo 'Оплата карточкой';
              if ($order->get_payment_method() == '') echo 'Наложенный платеж';
              ?>
            </span>
          </td>
        </tr>

        <tr>
          <td style="width: 30%;">Товары</td>
          <td>
            <?php foreach ($order->get_items() as $orderItem) : ?>
              <?php
              /**
               * @var $orderItem WC_Order_Item_Product
               */
              ?>
              <div>
                <?php echo $orderItem->get_name() ?> - x<?php echo $orderItem->get_quantity() ?> - <?php echo $orderItem->get_total() . ' ' . get_woocommerce_currency_symbol(); ?></div>
            <?php endforeach; ?>
          </td>
        </tr>

        </tbody>
      </table>
    </div>
    <?php endforeach; ?>
<?php else: ?>

<div>Список пуст</div>

<?php endif; ?>

