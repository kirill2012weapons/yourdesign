<?php

use KoshTheme\Forms\User\Checkout\CheckoutModel;
use KoshTheme\Forms\User\Checkout\CheckoutType;


$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
/**
 * @var $formFactory \Symfony\Component\Form\FormFactory
 */
$formFactory = container()->get('factory.form.annotation_validator');

$checkoutModel = new CheckoutModel();

if (is_user_logged_in()) {
  $curUser = wp_get_current_user();
  $currUserMeta = (object) get_user_meta($curUser->ID);
  if (isset($currUserMeta->first_name)) $checkoutModel->setFirstName($currUserMeta->first_name[0]);
  if (isset($currUserMeta->last_name)) $checkoutModel->setSecondName($currUserMeta->last_name[0]);
  if (isset($currUserMeta->billing_phone)) $checkoutModel->setPhone($currUserMeta->billing_phone[0]);
  $checkoutModel->setEmailAddress($curUser->get('user_email'));
}

$checkoutForm = $formFactory
  ->createBuilder(CheckoutType::class, $checkoutModel)
  ->getForm();

$checkoutForm->handleRequest($request);

if ($checkoutForm->isSubmitted() && $checkoutForm->isValid() && (WC()->cart->get_cart_contents_count() > 0)) {

  $products = WC()->cart->get_cart();
  $address = [
    'first_name' => $checkoutModel->getFirstName(),
    'last_name' => $checkoutModel->getSecondName(),
    'email' => $checkoutModel->getEmailAddress(),
    'phone' => $checkoutModel->getPhone(),
  ];

  try {
    $nvpoch = container()->get('factory.novaya_pochta');
  } catch (Exception $e) {
    logCustom($e->getMessage());
  }

  $cityData = $nvpoch->getCities( 0, '', $checkoutModel->getShippingCountry() )['data'][0]['DescriptionRu'];

  $w = $nvpoch->getWarehouses( $checkoutModel->getShippingCountry() );
  $nameWHouse = '';
  foreach ($w['data'] as $w_house) {
    if ($w_house['Ref'] == $checkoutModel->getShippingAddress1()) {
      $nameWHouse = $w_house['DescriptionRu'];
      break;
    }
  }

  if (is_user_logged_in()) {
    $order = wc_create_order(['customer_id' => get_current_user_id()]);
  } else {
    $order = wc_create_order();
  }

  $order->set_address($address);
  try {
    $order->set_shipping_address_1( $nameWHouse );
    $order->set_shipping_country('Украина');
    $order->set_shipping_city( $cityData );
    $order->set_customer_note( $checkoutModel->getAdditionInformation() );
  } catch (WC_Data_Exception $e) {
    logCustom($e->getMessage());
  }

  foreach ($products as $hash => $values) {
    try {
      $order->add_product($values['data'], $values['quantity']);
    } catch (WC_Data_Exception $e) {
      logCustom($e->getMessage());
    }
  }

  switch ($checkoutModel->getPaymentMethod()) {
    case 'nal_pay':

      $order->calculate_totals();
      $order->update_status('completed', 'Заказ <span style="font-weight: 700; color: #09a600;">НАЛОЖЕННЫЙ ПЛАТЕЖ</span>' . '<br>');
      WC()->cart->empty_cart();
      sendMailCustomOrder($order);

      container()->get('telega.order')->sendOrderToSales($order->get_id());

      wp_redirect(home_url('/checkout/order-received/' . $order->get_id() . '/?key=' . $order->get_order_key()));
      exit();

      break;
    case 'card_pay':

      $paymentGateways = WC()->payment_gateways()->payment_gateways();
      try {
        $order->set_payment_method('liqpay-webplus');
      } catch (WC_Data_Exception $e) {
        logCustom($e->getMessage());
      }

      $order->calculate_totals();
      WC()->session->order_awaiting_payment = $order->get_id();
      $result = $paymentGateways['liqpay-webplus']->process_payment($order->get_id());

      if ($result['result'] == 'success') {
        wp_redirect($result['redirect']);
        exit();
      }

      break;
  }

}

$checkoutFormView = $checkoutForm->createView();

$items = WC()->cart->get_cart();

?>

<form class="ps-checkout__form" action="?" method="post">
  <div class="row">
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 ">
      <div class="ps-checkout__billing">
        <h3>ПЛАТЕЖНАЯ ИНФОРМАЦИЯ</h3>


        <div class="form-group form-group--inline">
          <label>Имя<span>*</span>
          </label>
          <?php if ($checkoutFormView->children['firstName']->vars['errors']->count() > 0) : ?>
            <?php foreach ($checkoutFormView->children['firstName']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>
          <input class="form-control"
                 name="<?= $checkoutFormView->children['firstName']->vars['full_name'] ?>"
                 value="<?= $checkoutFormView->children['firstName']->vars['value'] ?>"
                 type="text">
        </div>


        <div class="form-group form-group--inline">
          <label>Фамилия<span>*</span>
          </label>
          <?php if ($checkoutFormView->children['secondName']->vars['errors']->count() > 0) : ?>
            <?php foreach ($checkoutFormView->children['secondName']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>
          <input class="form-control"
                 name="<?= $checkoutFormView->children['secondName']->vars['full_name'] ?>"
                 value="<?= $checkoutFormView->children['secondName']->vars['value'] ?>"
                 type="text">
        </div>


        <div class="form-group form-group--inline">
          <label>Email Адресс<span>*</span>
          </label>
          <?php if ($checkoutFormView->children['emailAddress']->vars['errors']->count() > 0) : ?>
            <?php foreach ($checkoutFormView->children['emailAddress']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>
          <input class="form-control"
                 name="<?= $checkoutFormView->children['emailAddress']->vars['full_name'] ?>"
                 value="<?= $checkoutFormView->children['emailAddress']->vars['value'] ?>"
                 type="email">
        </div>


        <div class="form-group form-group--inline">
          <label>Телефон<span>*</span>
          </label>
          <?php if ($checkoutFormView->children['phone']->vars['errors']->count() > 0) : ?>
            <?php foreach ($checkoutFormView->children['phone']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>
          <input class="form-control"
                 data-phone
                 name="<?= $checkoutFormView->children['phone']->vars['full_name'] ?>"
                 value="<?= $checkoutFormView->children['phone']->vars['value'] ?>"
                 type="text">
        </div>


        <h2>ДОСТАВКА ПО НОВОЙ ПОЧТЕ</h2><br>

        <div class="form-group form-group--inline">
          <label for="np_billing_country">Населенный пункт<span>*</span>
          </label>
          <?php if ($checkoutFormView->children['shippingCountry']->vars['errors']->count() > 0) : ?>
            <?php foreach ($checkoutFormView->children['shippingCountry']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>
          <select name="<?= $checkoutFormView->children['shippingCountry']->vars['full_name'] ?>" id="np_billing_country">
            <?php if (empty($checkoutFormView->children['shippingCountry']->vars['value'])) : ?>
              <option selected value="">Выберете населенный пункт</option>
            <?php endif; ?>
            <?php foreach ($checkoutFormView->children['shippingCountry']->vars['choices'] as $choice) : ?>
              <?php if ($checkoutFormView->children['shippingCountry']->vars['value'] == $choice->value) : ?>
                <option selected value="<?php echo $choice->value; ?>"><?php echo $choice->label; ?></option>
              <?php else: ?>
                <option value="<?php echo $choice->value; ?>"><?php echo $choice->label; ?></option>
              <?php endif; ?>
            <?php endforeach; ?>
          </select>
        </div>

        <div class="form-group form-group--inline">
          <label for="np_billing_country_part">Отделение НП<span>*</span>
          </label>
          <?php if ($checkoutFormView->children['shippingAddress1']->vars['errors']->count() > 0) : ?>
            <?php foreach ($checkoutFormView->children['shippingAddress1']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>
          <img data-trigger-load-parts-nv style="height: 80px; display: none;" src="<?php echo PHP_IMG; ?>/loadearth.svg" alt="">
          <select name="<?= $checkoutFormView->children['shippingAddress1']->vars['full_name'] ?>" id="np_billing_country_part">
            <?php if (empty($checkoutFormView->children['shippingAddress1']->vars['value'])) : ?>
              <option selected value="">Выберете населенный пункт</option>
            <?php endif; ?>
            <?php if (isset($checkoutFormView->children['shippingAddress1']->vars['choices'])) : ?>
              <?php foreach ($checkoutFormView->children['shippingAddress1']->vars['choices'] as $choice) : ?>
                <?php if ($checkoutFormView->children['shippingAddress1']->vars['value'] == $choice->value) : ?>
                  <option selected value="<?php echo $choice->value; ?>"><?php echo $choice->label; ?></option>
                <?php else: ?>
                  <option value="<?php echo $choice->value; ?>"><?php echo $choice->label; ?></option>
                <?php endif; ?>
              <?php endforeach; ?>
            <?php endif; ?>
          </select>
        </div>

        <h2>ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ</h2><br>


        <div class="form-group form-group--inline textarea">
          <label>Заметка</label>
          <?php if ($checkoutFormView->children['additionInformation']->vars['errors']->count() > 0) : ?>
            <?php foreach ($checkoutFormView->children['additionInformation']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>
          <textarea name="<?= $checkoutFormView->children['additionInformation']->vars['full_name'] ?>"
                    class="form-control"
                    rows="5"
                    placeholder=""><?= $checkoutFormView->children['additionInformation']->vars['value'] ?></textarea>
        </div>


      </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">

      <?php echo render('checkout/base_wrap.php', [
        'checkoutFormView' => $checkoutFormView,
        'items' => $items
      ]) ?>

      <div class="ps-shipping">
        <h3>ДОСТАВКА НОВОЙ ПОЧТОЙ</h3>
        <p>40 гривен.</p>
      </div>
    </div>
  </div>
</form>