<?php

namespace KoshTheme\Validator\Constraints;


use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

/**
 * @Annotation
 */
class ContainsUserNickNameExistsValidator extends ConstraintValidator
{
  public function validate($value, Constraint $constraint) {
    if (!$constraint instanceof ContainsUserNickNameExists) {
      return;
    }

    if (null === $value || '' === $value) {
      return;
    }

    $userInfo = wp_get_current_user();

    if ($userInfo->user_login == $value) return;

    if (username_exists($value)) $this->context
      ->buildViolation($constraint->message)
      ->addViolation();
    else return;

  }
}