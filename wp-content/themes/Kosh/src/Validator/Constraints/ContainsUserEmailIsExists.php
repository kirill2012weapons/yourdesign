<?php

namespace KoshTheme\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
* @Annotation
*/
class ContainsUserEmailIsExists extends Constraint
{
    public $message = 'Email is Exists';
}