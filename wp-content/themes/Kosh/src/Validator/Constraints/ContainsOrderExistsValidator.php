<?php

namespace KoshTheme\Validator\Constraints;


use KoshTheme\Forms\User\Checkout\CheckoutFindOrderModel;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsOrderExistsValidator extends ConstraintValidator
{

  public function validate($protocol, Constraint $constraint) {
    if (!$constraint instanceof ContainsOrderExists) {
      return;
    }

    /**
     * @var $protocol CheckoutFindOrderModel
     */

    $findedId = wc_get_order_id_by_order_key( $protocol->getOrderHash() );

    if ($findedId != $protocol->getOrderId())
      $this->context->buildViolation($constraint->message)
        ->addViolation();
    else return;

  }
}