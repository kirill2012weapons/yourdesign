<?php

namespace KoshTheme\Validator\Constraints;


use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

/**
* @Annotation
*/
class ContainsUserEmailIsExistsValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ContainsUserEmailIsExists) {
            return;
        }

        if (null === $value || '' === $value) {
            return;
        }

        $user = get_user_by( 'email', $value );

        if ($user) $this->context->buildViolation($constraint->message)
            ->addViolation();
        else return;

    }
}