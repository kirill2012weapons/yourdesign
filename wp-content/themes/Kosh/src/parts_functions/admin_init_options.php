<?php
class MySettingsPage
{
  /**
   * Holds the values to be used in the fields callbacks
   */
  private $options;

  /**
   * Start up
   */
  public function __construct()
  {
    add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
  }

  /**
   * Add options page
   */
  public function add_plugin_page()
  {
    add_menu_page(
      'Settings Telegram',
      'TELEGA',
      'manage_options',
      'telegram-settings',
      array( $this, 'create_admin_page' )
    );
  }

  public function create_admin_page()
  {

    echo render('admin/option/telegram.php');

  }

}

if( is_admin() )
  $my_settings_page = new MySettingsPage();