<?php

define('HTML_IMG',          get_theme_root_uri() . '/Kosh_html/images');
define('HTML_JS',           get_theme_root_uri() . '/Kosh_html/js');
define('HTML_CSS',          get_theme_root_uri() . '/Kosh_html/css');
define('HTML_PLUGINS',      get_theme_root_uri() . '/Kosh_html/plugins');
define('HTML_PLUGINS_FILE_SYSTEM',      ROOT . '/wp-content/themes/Kosh_html/plugins');
define('HTML_RESOURCES',    get_theme_root_uri() . '/Kosh_html/resources');

define('PHP_IMG',          get_theme_root_uri() . '/Kosh/assets/images');
define('PHP_JS',           get_theme_root_uri() . '/Kosh/assets/js');
define('PHP_JS_FILE_SYSTEM',           ROOT . '/wp-content/themes/Kosh/assets/js');
define('PHP_CSS',          get_theme_root_uri() . '/Kosh/assets/css');