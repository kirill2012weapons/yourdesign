<?php

add_action( 'wp_enqueue_scripts', 'addThemeScripts' );
function addThemeScripts()
{

//    wp_enqueue_script( 'jquery_min_js',                                 HTML_PLUGINS . '/jquery/dist/jquery.min.js',                                                            [], 1.0, true);
//    wp_enqueue_script( 'bootstrap_min_js',                              HTML_PLUGINS . '/bootstrap/dist/js/bootstrap.min.js',                                                   [], 1.0, true);
//    wp_enqueue_script( 'jquery_barrating_min_js',                       HTML_PLUGINS . '/jquery-bar-rating/dist/jquery.barrating.min.js',                                       [], 1.0, true);
//    wp_enqueue_script( 'owl_carousel_min_js',                           HTML_PLUGINS . '/owl-carousel/owl.carousel.min.js',                                                     [], 1.0, true);
//    wp_enqueue_script( 'jquery_lazy_min_js',                            PHP_JS       . '/jquery.lazy.min.js',                                                                   [], 1.0, true);
//    wp_enqueue_script( 'imagesloaded_pkgd_js',                          HTML_PLUGINS . '/imagesloaded.pkgd.js',                                                                 [], 1.0, true);
//    wp_enqueue_script( 'isotope_pkgd_min_js',                           HTML_PLUGINS . '/isotope.pkgd.min.js',                                                                  [], 1.0, true);
//    wp_enqueue_script( 'bootstrap_select_min_js',                       HTML_PLUGINS . '/bootstrap-select/dist/js/bootstrap-select.min.js',                                     [], 1.0, true);
//    wp_enqueue_script( 'jquery_matchHeight_min_js',                     HTML_PLUGINS . '/jquery.matchHeight-min.js',                                                            [], 1.0, true);
//    wp_enqueue_script( 'slick_min_js',                                  HTML_PLUGINS . '/slick/slick/slick.min.js',                                                             [], 1.0, true);
//    wp_enqueue_script( 'jquery_elevatezoom_js',                         HTML_PLUGINS . '/elevatezoom/jquery.elevatezoom.js',                                                    [], 1.0, true);
//    wp_enqueue_script( 'jquery_magnific_popup_min_js',                  HTML_PLUGINS . '/Magnific-Popup/dist/jquery.magnific-popup.min.js',                                     [], 1.0, true);
//    wp_enqueue_script( 'jquery_ui_min_js',                              HTML_PLUGINS . '/jquery-ui/jquery-ui.min.js',                                                           [], 1.0, true);
//    wp_enqueue_script( 'maps_googleapis_com',                           'https://maps.googleapis.com/maps/api/js?key=AIzaSyAx39JFH5nhxze1ZydH-Kl8xXM3OK4fvcg&amp;region=GB',    [], 1.0, true);
//    wp_enqueue_script( 'jquery_themepunch_tools_min_js',                HTML_PLUGINS . '/revolution/js/jquery.themepunch.tools.min.js',                                         [], 1.0, true);
//    wp_enqueue_script( 'jquery_themepunch_revolution_min_js',           HTML_PLUGINS . '/revolution/js/jquery.themepunch.revolution.min.js',                                    [], 1.0, true);
//    wp_enqueue_script( 'revolution_extension_video_min_js',             HTML_PLUGINS . '/revolution/js/extensions/revolution.extension.video.min.js',                           [], 1.0, true);
//    wp_enqueue_script( 'revolution_extension_slideanims_min_js',        HTML_PLUGINS . '/revolution/js/extensions/revolution.extension.slideanims.min.js',                      [], 1.0, true);
//    wp_enqueue_script( 'revolution_extension_layeranimation_min_js',    HTML_PLUGINS . '/revolution/js/extensions/revolution.extension.layeranimation.min.js',                  [], 1.0, true);
//    wp_enqueue_script( 'revolution_extension_navigation_min_js',        HTML_PLUGINS . '/revolution/js/extensions/revolution.extension.navigation.min.js',                      [], 1.0, true);
//    wp_enqueue_script( 'revolution_extension_parallax_min_js',          HTML_PLUGINS . '/revolution/js/extensions/revolution.extension.parallax.min.js',                        [], 1.0, true);
//    wp_enqueue_script( 'revolution_extension_actions_min_js',           HTML_PLUGINS . '/revolution/js/extensions/revolution.extension.actions.min.js',                         [], 1.0, true);


//    wp_enqueue_script( 'bootbox_min_js',                                PHP_JS       . '/bootbox.min.js',                                                                       [], 1.0, true);
//    wp_enqueue_script( 'jquery_magnific_popup_min_js',                  PHP_JS       . '/jquery.magnific-popup.min.js',                                                         [], 1.0, true);
//    wp_enqueue_script( 'selectsize_js',                                 PHP_JS       . '/selectsize.js',                                                                        [], 1.0, true);
//    wp_enqueue_script( 'inputmask_min_js',                              PHP_JS       . '/inputmask.min.js',                                                                     [], 1.0, true);

    wp_enqueue_script( 'compiledfirstpartjsminjs',                      PHP_JS . '/compiled-first-part-js.min.js',                                                              [], '1.0', true);

    wp_enqueue_script( 'main_js',                                       HTML_JS      . '/main.js',                                                                              [], '1.0', true);
    wp_enqueue_script( 'custom_js',                                     PHP_JS       . '/custom.js',                                                                            [], '1.0', true);

    wp_enqueue_style( 'font_awesome_min_css',               HTML_PLUGINS . '/font-awesome/css/font-awesome.min.css',                [], '1.0', 'all');
    wp_enqueue_style( 'ps_icon_style_css',                  HTML_PLUGINS . '/ps-icon/style.css',                                    [], '1.0', 'all');
    wp_enqueue_style( 'bootstrap_min_css',                  HTML_PLUGINS . '/bootstrap/dist/css/bootstrap.min.css',                 [], '1.0', 'all');
    wp_enqueue_style( 'owl_carousel_css',                   HTML_PLUGINS . '/owl-carousel/assets/owl.carousel.css',                 [], '1.0', 'all');
    wp_enqueue_style( 'fontawesome_stars_css',              HTML_PLUGINS . '/jquery-bar-rating/dist/themes/fontawesome-stars.css',  [], '1.0', 'all');
    wp_enqueue_style( 'slick_css',                          HTML_PLUGINS . '/slick/slick/slick.css',                                [], '1.0', 'all');
    wp_enqueue_style( 'bootstrap_select_min_css',           HTML_PLUGINS . '/bootstrap-select/dist/css/bootstrap-select.min.css',   [], '1.0', 'all');
    wp_enqueue_style( 'magnific_popup_css',                 HTML_PLUGINS . '/Magnific-Popup/dist/magnific-popup.css',               [], '1.0', 'all');
    wp_enqueue_style( 'jquery_ui_min_css',                  HTML_PLUGINS . '/jquery-ui/jquery-ui.min.css',                          [], '1.0', 'all');
    wp_enqueue_style( 'settings_css',                       HTML_PLUGINS . '/revolution/css/settings.css',                          [], '1.0', 'all');
    wp_enqueue_style( 'layers_css',                         HTML_PLUGINS . '/revolution/css/layers.css',                            [], '1.0', 'all');
    wp_enqueue_style( 'navigation_css',                     HTML_PLUGINS . '/revolution/css/navigation.css',                        [], '1.0', 'all');
    wp_enqueue_style( 'css_style_css',                      HTML_CSS     . '/style.css',                                            [], '1.0', 'all');
    wp_enqueue_style( 'selectsize_css',                     PHP_CSS      . '/selectsize.css',                                       [], '1.0', 'all');
    wp_enqueue_style( 'magnific_popup_css',                 PHP_CSS      . '/magnific-popup.css',                                   [], '1.0', 'all');

    wp_enqueue_style( 'custom_css_my',                      PHP_CSS      . '/custom.css',                                           [], '1.0', 'all');

}


/**
 * MINIFY ALL JS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

//use Assetic\Asset\AssetCollection;
//use Assetic\Asset\FileAsset;
//use Assetic\Filter\UglifyJs2Filter;
//use Assetic\Filter\UglifyJsFilter;
//
//$jsAsset = new AssetCollection(array(
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/jquery/dist/jquery.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/bootstrap/dist/js/bootstrap.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/jquery-bar-rating/dist/jquery.barrating.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/owl-carousel/owl.carousel.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(PHP_JS_FILE_SYSTEM . '/jquery.lazy.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/imagesloaded.pkgd.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/isotope.pkgd.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/bootstrap-select/dist/js/bootstrap-select.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/jquery.matchHeight-min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/slick/slick/slick.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/elevatezoom/jquery.elevatezoom.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/Magnific-Popup/dist/jquery.magnific-popup.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/jquery-ui/jquery-ui.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//
//
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/revolution/js/jquery.themepunch.tools.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/revolution/js/jquery.themepunch.revolution.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/revolution/js/extensions/revolution.extension.video.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/revolution/js/extensions/revolution.extension.slideanims.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/revolution/js/extensions/revolution.extension.layeranimation.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/revolution/js/extensions/revolution.extension.navigation.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/revolution/js/extensions/revolution.extension.parallax.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(HTML_PLUGINS_FILE_SYSTEM . '/revolution/js/extensions/revolution.extension.actions.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//
//  new FileAsset(PHP_JS_FILE_SYSTEM . '/bootbox.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(PHP_JS_FILE_SYSTEM . '/jquery.magnific-popup.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(PHP_JS_FILE_SYSTEM . '/selectsize.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//  new FileAsset(PHP_JS_FILE_SYSTEM . '/inputmask.min.js', [new UglifyJs2Filter('C:\\Users\\Kirill\\AppData\\Roaming\\npm\\node_modules\\uglify-js\\bin\\uglifyjs', 'C:\\Program Files\\nodejs\\node')]),
//));
//
//file_put_contents(ROOT . '/wp-content/themes/Kosh/assets/js/compiled-first-part-js.min.js', ($jsAsset->dump()));


function wpdocs_selectively_enqueue_admin_script( $hook ) {
  if (isset($_GET['page']) && ($_GET['page'] == 'telegram-settings')) {
    wp_enqueue_style( 'bootstrap_tagsinput_css',            PHP_CSS       . '/bootstrap-tagsinput.css',                             [], '1.0', 'all');
  }
  if (isset($_GET['page']) && ($_GET['page'] == 'telegram-settings')) {
    wp_enqueue_script( 'tags_bt_min_js',                    PHP_JS       . '/bootstrap-tagsinput.min.js',                           [], 1.0, true);
  }
}
add_action( 'admin_enqueue_scripts', 'wpdocs_selectively_enqueue_admin_script' );
