<?php
/**
 * Template Name: Register Page
 */
?>

<?php
if (is_user_logged_in()) {
  wp_safe_redirect( home_url() );
  die();
}
?>

<?php get_header(); ?>

<div class="ps-contact ps-section pb-80">
    <div class="ps-container">

        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                <div class="ps-section__header mb-50">
                    <h2 class="ps-section__title" data-mask="Log IN">- Личный кабинет</h2>

                    <?php get_template_part('src/templates/users-form/form', 'login'); ?>

                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                <div class="ps-section__header mb-50">
                    <h2 class="ps-section__title" data-mask="Register">- Регистрация</h2>

                    <?php get_template_part('src/templates/users-form/form', 'register'); ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
