<?php
/**
 * Template Name: Checkout Page Custom
 */
?>

<?php get_header(); ?>

<div class="ps-checkout pt-80 pb-80">
  <div class="ps-container">

    <?php if (is_wc_endpoint_url('order-received')) : ?>

      <?php
      global $wp;
      $orderId = absint($wp->query_vars['order-received']);
      $order = wc_get_order($orderId);
      echo render('checkout/thankyou.php', [
        'order' => $order,
        'orderHash' => isset($_GET['key']) ? $_GET['key'] : '',
      ]);
      ?>

    <?php else: ?>

      <?php echo render('checkout/payment_form.php'); ?>

    <?php endif; ?>

  </div>
</div>

<?php get_footer(); ?>
